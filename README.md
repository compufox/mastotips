# ✨ soff ✨ masto tips

A repository to help you brain Masto better when you're installing it.


## Small "gotchas"

These are things to keep in mind when editing or recompiling things.

  * `RAILS_ENV` -- When you run stuff, you'll want to use `RAILS_ENV=production`
    or a similar `RAILS_ENV` environment variable (development?) for your
    command.

  * `NODE_OPTIONS` -- This is an option passed to Node you can use to increase
    the maximum usable memory. By default, Node will only use something like
    ~1.7GiB of memory for compiling, but you can use the parameter
    `--max-old-space-size` to specify in MB the maximum memory. An example:

```
# Inside your main production mastodon directory, like /home/mastodon/live
$ NODE_OPTIONS='--max-old-space-size=4096' RAILS_ENV=production bundle exec rails assets:precompile
```

## Utilities

A list of utilities to help with routine maintainence within your instance.

  * `tootctl` -- This is your swiss army knife. You can modify account
    properties, import emojo, manage/clean media (images, video, etc), and other
    stuff. See `tootctl --help` for an more info on that. This can be found in
    `$MASTODON/bin`. When you run it, you will want to use
    `RAILS_ENV=$whatever_you_are_using`. In most cases, you'll want `production`
    there. Otherwise, Ruby will scream at you. Like, a frickin' lot.


## Files and directories

This is a small list of files you can use to edit the overall layout and
functionality of your instance.


### Files

  * `show.html.haml` -- This file resides in
    `$MASTODON/app/views/settings/profiles`. Use it to adjust (for example)
    character count labels in the profile window in user settings. (This does
    not actually modify the character limit. See the two below files.)

  * `settings.js` (Glitch-Soc) -- Change the bio and display name limit. This
    file resides in `$MASTODON/app/javascript/core/`. For vanilla Mastodon,
    this can be changed in `$MASTODON/app/javascript/packs/public.js`. You'll
    want to edit the line `nameCounter.textContent = ...` (or similar) in either
    case.

  * `account.rb` -- This file is responsible for certain account actions like
    character length validation. It resides in `$MASTODON/app/models/`. Edit
    `MAX_NOTE_LENGTH` and the line `validates :display_name ...` accordingly.


### Directories

  * `config/` -- This is where your (you guessed it!) configuration will sit.
    Inside you will find:

    * `locales/` -- This will have your locales (pfft). This directory also
      contains your `devise` files (`devise.en.yml`, `devise.es.yml`, etc) and
      similar files. These are useful to change what your email text will say
      to new instance members.

    * `settings.yml` -- Some default account settings. In particular, whether to
      allow search engine indexing by default (`noindex`), site title, reserved
      user names, and default selection for notification emails.

    * `puma.rb` -- For your web services (separate from Nginx).


## License

I never know how to license things like this. In this particular case, I've
decided on public domain. This is my repository, but the information contained
within it is for the people. It is made _by_ the people. The people are who made
Mastodon. I am but one person in that very large crowd. Read about
[CC0](https://creativecommons.org/publicdomain/zero/1.0) for more information.
